package omkar;
public class Arrays3 {
    public static void main(String[] args) {
        int[] arr={1,2,3,4,5,6,7,8,10};
        for (int a:arr){
            System.out.print(a+" ");
        }
        System.out.println();
        int n= arr.length;
        int sum=((n+1)*(n+2))/2;
        for (int a:arr){
            sum=sum-a;
        }
        System.out.println(sum);
    }
}