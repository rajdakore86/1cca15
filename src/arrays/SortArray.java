package arrays;

public class SortArray {
    public static void main(String[] args) {
        int[]arr=RandomArray.randomArr(10);
        for (int a:arr)
            System.out.print(a+" ");
        for (int i=1;i< arr.length;i++){
            int key=arr[i];
            int j=i-1;
            while (j>=0 && arr[j]>key){
                arr[j+1]=arr[j];
                j-=1;
            }
            arr[j+1]=key;
        }
        System.out.println();
        System.out.println();
        for (int a:arr)
            System.out.print(a+" ");
    }
}
