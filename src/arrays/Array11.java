package arrays;

import java.util.Scanner;

public class Array11 {
    public static void main(String[] args) {
        int[] arr = RandomArray2.randomArr(10);

        for (int a : arr)
            System.out.print(a + " ");
        System.out.println();

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++)
                if (arr[i] == arr[j])
                    System.out.println(arr[i]);
        }
    }
}
