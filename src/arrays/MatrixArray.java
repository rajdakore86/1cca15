package arrays;

import java.util.Scanner;

public class MatrixArray {
    public static void main(String[] args) {

        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter Row Value");
        int row = sc1.nextInt();
        System.out.println("Enter Column Value");
        int col = sc1.nextInt();
        int[][] arr = new int[row][col];
        int temp[][] = new int[row][col];
        System.out.println("Enter Matrix Values ");

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                arr[i][j] = sc1.nextInt();
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                temp[i][j] = arr[i][j];
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.print(temp[i][j] + "\t");
            }
            System.out.println();
        }
    }
}