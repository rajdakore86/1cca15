package patterns;

public class ReverseNumber1 {
    public static void main(String[] args) {
        int a=1234;
        while (a!=0){
            int r=a%10;
            System.out.print(r);
            a=a/10;
        }
    }
}
