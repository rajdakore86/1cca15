package patterns;

public class pattern42 {
    public static void main(String[] args) {
        int line = 7;
        int star = 1;
        int space=3;
        int ch=0;
        for (int i = 0; i < line; i++) {
            for (int k=0;k<space;k++)
                System.out.print(" ");

            for (int j = 0; j < star; j++)
            if (i <j)
                System.out.print(ch--);
            else
                System.out.print(ch++);
            }
        }
    }