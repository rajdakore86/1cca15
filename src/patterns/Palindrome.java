package patterns;

public class Palindrome {
    public static void main(String[] args) {
        for (int i = 10; i <= 10000; i++) {
            int a = i, temp=a;
            int sum = 0;
            while (a != 0) {
                int r = a % 10;
                sum = (sum * 10) + r;
                a /= 10;
            }
            if (sum == temp) {
                System.out.println(sum);
            }
        }
    }
}