package patterns;

public class pattern12 {
    public static void main(String[] args) {
        int line=4;
        int star=3;
        char ch='A';
        for (int i=0;i<line;i++){
            for (int j=0;j<star;j++){
                System.out.print(ch+"\t");
                ch++;
            }
            System.out.println();
        }
    }
}
