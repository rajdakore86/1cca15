package patterns;

public class pattern22 {
    public static void main(String[] args) {
        int line=5;
        int star=4;
        int ch=1;
        for (int i=0;i<line;i++){
            for (int j=0;j<star;j++){
                if (ch==2 || ch==4){
                System.out.print("*" +" ");
                } else{
                    System.out.print(ch+" ");
                }
            }
            System.out.println();
            ch++;
        }
    }
}
