package patterns;

public class SpyNumber2 {
    public static void main(String[] args) {
        int a=1124;
        int mul=1;
        int sum=0;

        while (a!=0){
            int r=a%10;
            sum+=r;   //sum=sum+r
            mul*=r;  //mul=mul*r
            a=a/10;
        }
        if (sum==mul)
            System.out.println("Spy Number");
        else
            System.out.println("Not a Spy Number");
    }
}
