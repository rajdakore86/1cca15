package patterns;

public class ZeroNumber {
    public static void main(String[] args) {
        long a=110000102500087700l;
        int count=0;

        while (a!=0){
            int r=(int)(a%10);
            if (r==0)
                count++;
            a=a/10;
        }
        System.out.println(count);
    }
}
