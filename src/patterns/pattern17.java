package patterns;

public class pattern17 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        for (int i = 0; i < line; i++) {
        int ch=1;
            for (int j = 0; j < star; j++) {
                System.out.print(ch++ + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }
}
