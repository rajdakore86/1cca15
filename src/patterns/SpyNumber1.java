package patterns;

public class SpyNumber1 {
    public static void main(String[] args) {
        for (int i = 10; i < 100000; i++) {
            int a = i;
            int sum = 0;
            int mul = 1;

            while (a != 0) {
                int r = a % 10;
                sum+=r;  //sum=sum+r
                mul*=r;  //mul=mul*r
                a=a/10;
            }
            if (sum==mul)
                System.out.println(i);
        }
    }
}
