package patterns;

public class pattern8 {
    public static void main(String[] args) {
        int line = 10;
        int star = 10;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if (i == j || i == 4 || j == 4)
                    System.out.print(" *");
                else
                    System.out.print("  ");
            }
            System.out.println();
        }
    }
}