package patterns;

public class pattern21 {
    public static void main(String[] args) {
        int line=4;
        int star=4;
        int ch='A';
        for (int i=0;i<line;i++){
            for (int j=0;j<star;j++){
                System.out.print(ch++ +"\t");
                if (ch>'E'){
                    ch='A';
                }
            }
            System.out.println();
        }
    }
}
