package patterns;

public class ArmStrong1 {
    public static void main(String[] args) {
      for (int i=10;i<=10000;i++){
          int a=i,temp=a;
          int sum=0,count=0;

          while (temp!=0) {
              temp /= 10;
              count++;
          }
          while (a!=0){
              int r=a%10;
              sum=(int)(sum+Math.pow(r,count));
              a/=10;
          }
          if (sum==i)
              System.out.println(sum);
      }
    }
}
