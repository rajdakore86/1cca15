package patterns;

public class ArmStrong2 {
    public static void main(String[] args) {
        int a = 153;
        int sum = 0;
        int temp = a;

        while (a != 0) {
            int r = a % 10;
            sum += r * r * r;
            a = a / 10;
        }
        if (sum == temp)
            System.out.println("ArmStrong Number");
        else
            System.out.println("Not a ArmStrong Number");
            }
        }
