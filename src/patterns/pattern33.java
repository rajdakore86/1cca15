package patterns;

public class pattern33 {
    public static void main(String[] args) {
        int star = 5;
        int line = 5;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if (j == 0 || i == 4 || j == 4 || i == 0) {
                    System.out.print("*");
                } else {
                    System.out.println("");
                }
            }
        }
    }
}