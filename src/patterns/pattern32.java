package patterns;

public class pattern32 {
    public static void main(String[] args) {
        int star = 5;
        int line = 5;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                System.out.print("*\t");
            }
            System.out.println();
        }
    }
}
