package strings;

import java.util.HashSet;
import java.util.Set;

public class HashSet2 {
    public static void main(String[] args) {
        String str = "CORE JAVA PROGRAMMING";
        Set<Character> hs1 = new HashSet<>();

        for (int i = 0; i < str.length(); i++) {
            hs1.add(str.charAt(i));
        }
        for (Character c : hs1) {
            int count = 0;
            for (int i = 0; i < str.length(); i++) {
                if (c == str.charAt(i)) {
                    count++;
                }
            }
            System.out.println(c + " occurrences " + count);
        }
    }
}
