package strings;

import java.util.HashSet;
import java.util.Set;

public class HashSet3 {
    public static void main(String[] args) {
        String str = "CORE JAVA PROGRAMMING";
        Set<Character> hs1 = new HashSet<>();

        for (int i = 0; i < str.length(); i++) {
            hs1.add(str.charAt(i));
        }
        for (Character c : hs1) {
            System.out.println(c+"");
        }
    }
}
